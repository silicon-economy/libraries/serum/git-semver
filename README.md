# git-semver

## [DEPRECATED]

> This project is deprecated due to name conflict on [crates.io](https://crates.io/). \
> The new repository can be found here: \
> https://git.openlogisticsfoundation.org/silicon-economy/libraries/serum/git-tags-semver

Tool to extract SemVer Version Information from annotated git tags

This package allows to extract SemVer version information from annotated git tags in the format
"vX.Y.Z". It is used from `build.rs` build scripts. For usage details, see the API documentation.

## License

Open Logistics Foundation License\
Version 1.3, January 2023

See the LICENSE file in the top-level directory.

## Contact

Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>
