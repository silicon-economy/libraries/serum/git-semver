// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

#![no_std]

/// A SemVer (Semantic Version)
#[derive(Debug, Eq, PartialEq)]
pub struct SemanticVersion {
    /// Major release
    pub major: u32,
    /// Minor release
    pub minor: u32,
    /// Patch level
    pub patch: u32,
    /// Whether this was tagged as release candidate or not
    pub rc: bool,
    /// Amount of commits since this semver was tagged
    pub commits: u32,
}

/// Version information parsed from the git state
#[derive(Debug, Eq, PartialEq)]
pub struct GitVersion<'a> {
    /// Last Tag parsed as semantic version (if tagged at all)
    pub semver: Option<SemanticVersion>,
    /// The beginning of the unique git hash as byte array
    pub hash: [u8; 4],
    /// True if the repo contains uncommitted changes
    pub dirty: bool,
    /// The string returned by the git-describe call
    pub git_string: &'a str,
}
