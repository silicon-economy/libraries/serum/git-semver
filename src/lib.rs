// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

// When used in a build.rs, we use std features for convenience. When used in a (possibly no_std)
// firmware or application, this is a no_std lib which only defines the SemanticVersion and
// GitVersion types.
#![cfg_attr(not(feature = "build"), no_std)]
#![warn(missing_docs)]

//! Automatically Generate Semver and Git Information for your Project's Git Repo
//!
//! This crate is made to call and parse the git describe command. It is assumed that the repo uses
//! _annotated_ git tags in the format "vX.Y.Z" or "vX.Y.Z-rc". So for example, the first commit of
//! the release candidate branch for 1.0.0 should be tagged with "v1.0.0-rc" and the actual 1.0.0
//! release should be tagged with "v1.0.0". This information has to be extracted at compile time so
//! it needs to be done in a `build.rs`. Since the `build.rs` is run in the directory of the crate
//! it belongs to, your project itself should contain that `build.rs`, i.e. it should not be done
//! in a git submodule or a dependency of your project. The following `build.rs` will write the
//! version information to `version.rs` in the current `OUT_DIR` directory which is
//! ["the folder in which all output and intermediate artifacts should be placed"](https://doc.rust-lang.org/cargo/reference/environment-variables.html#environment-variables-cargo-sets-for-build-scripts)
//! by build scripts:
//!
//! ```
//! use std::env;
//! use std::fs;
//! use std::path::Path;
//! use std::process::Command;
//!
//! # // The test only works if the "build" feature is activated. To prevent the test from failing,
//! # // we add an empty main function below if it is not activated.
//! #
//! # #[cfg(feature = "build")]
//! fn main() {
//!     // Tell Cargo that if the given file changes, to rerun this build script (adjust the path
//!     // to the .git directory accordingly if required, e.g. when the crate is in a workspace)
//!     println!("cargo:rerun-if-changed=.git/index");
//!     println!("cargo:rerun-if-changed=.git/HEAD");
//!     println!("cargo:rerun-if-changed=.git/logs/HEAD");
//!     println!("cargo:rerun-if-changed=build.rs");
//!
//!     // Extract the version information and generate the corresponding code
//!     let out = git_semver::generate_version_file().unwrap();
//!
//!     # // We are not in a true build.rs here, so set the required environment variables manually
//!     # env::set_var("OUT_DIR", env::temp_dir());
//!     #
//!     // Write the generated version information code to version.rs in the OUT_DIR directory
//!     let out_dir = env::var_os("OUT_DIR").unwrap();
//!     let dest_path = Path::new(&out_dir).join("version.rs");
//!     fs::write(&dest_path, &out).unwrap();
//!
//!     // Touch build.rs to force recompilation on every workspace build. This may be desirable
//!     // because it ensures that the information is guaranteed to be up-to-date. On the other
//!     // hand, it may slow down your development process a tiny little bit because it enforces
//!     // a rebuild all the time. For example, your crate will be rebuilt before each `cargo run`
//!     // which does not happen normally (if the crate was built before, it can normally be run
//!     // immediately).
//!     Command::new("touch")
//!         .args(&["build.rs"])
//!         .output()
//!         .expect("Could not touch build.rs!");
//!     #
//!     # // Currently, this crate does not have a build.rs so we have to clean up after ourselves
//!     # Command::new("rm").args(&["build.rs"]).output()
//!     # .expect("Could not clean up, i.e. remove build.rs");
//! }
//!
//! # #[cfg(not(feature = "build"))]
//! # fn main() {}
//! ```
//!
//! Afterwards, `<OUT_DIR>/version.rs` contains a `pub const` [`GitVersion<'static>`](GitVersion).
//! Specifically, that file looks like:
//! ```
//! pub const VERSION: git_semver::GitVersion<'static> = git_semver::GitVersion { semver: Some(git_semver::SemanticVersion { major: 1, minor: 0, patch: 0, rc: false, commits: 0}), hash: [
//!     0x12,
//!     0x34,
//!     0xab,
//!     0xcd,
//! ], dirty: false, git_string: "v1.0.0-00-g1234abcd" };
//! ```
//!
//! To use it in your application, you can include the generated code like that:
//!
//! ```ignore
//! include!(concat!(env!("OUT_DIR"), "/version.rs"));
//!
//! fn main() {
//!     println!("{:?}", VERSION);
//! }
//! ```
//!
//! ## Cargo.toml / Feature-Gates
//! This tool is especially designed to be used in `no_std` contexts, e.g. bare-metal firmwares.
//! Therefore, it is generally `no_std` (the type definitions) but for extracting and writing the
//! version information to an intermediate file, it needs the standard library (and `build.rs` is
//! run with `std`, even for `no_std` packages). Thus, the standard library usage which enables the
//! part required in the `build.rs` has to be enabled with the "build" feature. So, for a `no_std`
//! package, you need to add it as a dependency twice, the corresponding part in your `Cargo.toml`
//! could look like this:
//! ```toml
//! [dependencies]
//! git-semver = { version = "1.0.0" }
//!
//! [build-dependencies]
//! git-semver = { version = "1.0.0", features = ["build"] }
//! ```
//!
//! Note that this additionally requires to use
//! [feature resolver version 2](https://doc.rust-lang.org/cargo/reference/resolver.html#feature-resolver-version-2)
//! for your project which is
//! [the default since Rust's 2021 edition](https://doc.rust-lang.org/cargo/reference/resolver.html#resolver-versions)
//! or can be specified with `resolver = "2"` in your `Cargo.toml` (if using a workspace, this must
//! be done in the top-level/workspace `Cargo.toml`).

mod types;
pub use types::{GitVersion, SemanticVersion};

#[cfg(feature = "build")]
mod generator;
#[cfg(feature = "build")]
pub use generator::{generate_version_file, VersionError};
